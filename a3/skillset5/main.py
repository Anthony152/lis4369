import functions as f

def main():
    f.get_requirements()
    num1, num2, oper = f.get_input()
    f.calculate(num1, num2, oper)
    
if __name__ == "__main__":
    main()