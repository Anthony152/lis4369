import datetime
# import pandas_datareader as pd # pandas = "Python data analysis library
import pandas_datareader as pdr 
import matplotlib.pyplot as plt
from matplotlib import style


def get_requirements():
    print("Data Analysis 1")
    print("\nProgram Requirements:\n"
    + "1. Run demo.py\n"
    + "2. If errors, more than likely missing installations.\n"
    + "3. Test Python Package Installer: pip freeze\n"
    + "4. Research how to do the following installations:\n"
    + "\ta. pandas (only if missing)\n"
    + "\tb. pandas-datareader (only if missing)\n"
    + "\tc. matplotlib (only if missing)\n"
    + "5. Create at least three functions that are called by the the program:\n"
    + "\ta. main(): calls at least two other functions:\n"
    + "\tb. get_requirements(): displays the program requirement.\n"
    + "\tc. data_analysis_1(): displays the following data.")

def data_analysis_1():
    start = datetime.datetime(2010, 1, 1)
    end = datetime.datetime.now()

    # Read data into pandas dataframe, implicitly created by datareader
    # Note: XOM is stock market symbol for exxon mobil corporation
    df = pdr.DataReader("XOM", "yahoo", start, end)

    print("\nPrint number of records: ")
    print(len(df))

    print("\nPrint columns: ")
    print(df.columns)

    print("\nPrint data frame: ")
    print(df) # Note: date is lower than the other columns as it is treated as as an index

    print("\nPrint first five lines:")
    print(df.head()) # head() prints top 5 rows. here with 7 columns

    print("\nPrint last five lines:")
    print(df.tail())

    print("\nPrint first 2 lines:")
    print(df.head(2))

    print("\nPrint last 2 lines:")
    print(df.tail(2))

    # Research what these styles do
    # styl.use('fivethirtyeight')
    style.use('ggplot')
    df['High'].plot()
    df['Adj Close'].plot()
    plt.legend()
    plt.show()