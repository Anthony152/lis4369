import functions as f

def main():
    f.get_requirements()
    f.volume_calculator()

if __name__ == "__main__":
    main()