def get_requirements():
    print("Developer: Anthony Threlfall")
    print("Python Looping structure")
    print("\nProgram Requirements:\n"
        + "1. Print while loop.\n"
        + "2. Print for loops using range() function, and implicit and explicit lists.\n"
        + "3. Use break and continue statements.\n"
        + "4. Replicate display below.\n"
        + "Note: In python, for loop used for iterating overa sequence (i.e., list, tuple, dictionary, st, or string).\n")

def print_loops():
# loop structure
# rang() function has two sets of parameters:
# 1. range(stop)
#stop: Number of intergers (whole numbers) to generate starting from zero Ex: range(3) == [0, 1, 2]

# 2. range([start], stop[, step])
# start: starting number
# stop: generate numbers up to but no including number
# step: difference btween each number in sequence

    print("1. while loop:")
    i = 1
    while i <= 3:
        print(i)
        #print(i, sep='', end=". flush=True) 
        i+=1  # see if this works

    print("\n2. for loop: using range() function with 1 arg")

    for i in range(4):
        print(i)

    print("\n3. for loop: using range() function with two args")
    for i in range(1, 4):
        print(i)
    
    print("\n4. for loop: using range() function with three args (interval 2)")
    #[1, 3]
    for i in range (1, 4, 2):
        print(i)

    print("\n5. for loop: using range() function with three args (negative interval:)")
    #interval can be negative
    #[3, 1]
    for i in range(3, 0, -2):
        print(i)

    print("\n6. for loop using (implicit) list (i.e, list not assigned to variable):") 
    #prints 1 - 3, including 1 and 3
    for i in [1, 2, 3]:
        print(i)

    print("\n7. for loop iterating through (explicit) string list:")
    states = ['Michigan', 'Alabama', 'Florida']
    for state in states:
        print(state)
    
    print("\n8. for loop using break statement (stops loop):")
    # break statement stops loop
    states = ['Michigan', 'Alabama', 'Florida']
    for state in states:
        if state == "Alabama":
            break
        print(state)
    
    print("\n9. for loop using continue statement (stops and continues with next):")
    # continue statement stops current iteration, and continues with next
    states = ['Michigan', 'Alabama', 'Florida']
    for state in states:
        if state == "Alabama":
            continue
        print(state)

    print("\n10. print list length:")
    states = ['Michigan', 'Alabama', 'Florida']
    print(len(states))

