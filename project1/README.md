> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Project 1 Requirements:

*Five Parts:*

1. Create Demo.py
2. Data Analysis 1 App
3. Complet Python skills set
4. Chapter Questions (Chs 7 and 8)
5. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running demo.py and code.
* Screenshot of running Data Analysis 1 app
* Screenshots of Python skill sets
* Link to project1.jpynb file: [data_analysis1.jpynb](data_analysis1/data_analysis1.ipynb "Project 1 Jupyter Notebook") 


#### Project Screenshot:

#### Demo.py

| Demo.py code | Running Demo.py | Demo.py graph |
| ------------ | --------------  | ------------- |
| ![Demo.py code](img/demo_code.png "Demo's Code") | ![Running Demo.py](img/running_demo.png "Running Demo.py") | ![Demo's graph](img/demo_graph.png "Demo's graph") |


#### Data Analysis 1 App
|               |              |                |
| ------------- | ------------ | -------------- |
| ![data analysis 1 App](img/data_analysis1.png "Data Analysis 1") | ![data analysis 1 app](img/data_analysis2.png "Data Analysis 1") | ![data analysis 1 graph](img/p1_graph.png "Data Analysis graph") |

*Screenshots of Python Skill Sets:*

| Skill Set 7: Using Lists | SkillSet 8: Using Tuples | Skill Set 9: Using Sets|
| ----- | ----- | ----- |
| ![Skill Set 7](img/ss7.png "Using Lists") | ![Skill Set 8](img/ss8.png "Using Tuples") | ![Skill Set 9](img/ss9.png "Using Sets") |

#### Project 1 Jupyter Notebook:
| ![data_analysis.ipynb](img/p1_jnotebook1.png "Project 1 Jupyter Notebook") | ![data_analysis.jpynb](img/p1_jnotebook2.png "Project 1 Jupyter Notebook") |
| ----- | ----- |
|       |       |
