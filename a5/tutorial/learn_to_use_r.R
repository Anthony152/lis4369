rm(list = ls(envir = globalenv()), envir = globalenv()); if(!is.null(dev.list())) dev.off(); gc(); cat("\014")

# data()
# barChart(AAPL)
# barChart(AAPL, subset='last 14 days')
# chartSeries(AAPL, subset'last 14 days')
# chartSeries(AAPL, subset='last 14 days')
# chartSeries(AAPL, subset='last 14 days')
# barChart(AAPL['2013-04-1::2013-04-12])
# rm(x)
# str(Titanic)
# colnames(Titanic)
# cor(USArrests)
# choose(15,4)
# mypeople <- c("Bob","Joane","Sally","Tim","Neal)
# combn(mypeople, 2)
# mtcars$mpg
# mtcars[,2:4]
# mtcars [,c(2,4)]
# mtcars$mpg>20
# mtcars[mtcars$mpg>20,]
# mtcars[mtcars$mpg>20,c(1,4)]
# mtcars[mtcars$mpg>20,c("mpg","hp")]
# mpg20 <- mtcars$mpg > 20
# cols <- c("mpg", "hp")
# mtcars[mpg20, cols]
# attach(mtcars)
# mpg <- mpg > 20
# detach()
# subset(mtcars, mpg>20, c("mpg", "hp"))
# subset(mtcars,mpg==max(mpg))
# subset(mtcars,mpg==max(mpg), mpg)
# subset(mtcars, , c("mpg", "hp"))
# subset(mtcars, select=c("mpg", "hp"))


#install.packages("dplyr", dependencies = TRUE)
#install.packages("ggplot2", dependencies = TRUE)
#update.packages(ask = FALSE, checkBuilt = TRUE)
#installed.packages()

library(dplyr)


filter(mtcars, mpg>20)

# mtcars % > % # not . in between
# filter(mpg > 20) % > %
# select(mpg, hp)
# arrange(desc(mpg))

# table(mtcars$mpg, mtcars$hp)
# plot mtcars$disp, mtcars$mpg)
#   xlab="Engine displacement",
#   ylab="mpg", main="MPG compared with
#   las=1)


library(ggplot2)
qplot(disp, mpg, data=mtcars)
qplot(disp, mpg, ylim=c(0,35), data=mtcars)
qplot(cty, hwy, data=mpg)
qplot(cty, hwy, data=mpg, geom="jitter")
ggplot(mtcars, aes(x=disp, y=mpg)) + geom_point()
ggplot(mtcars, aes(x=disp, y=mpg)) + geom_line()
ggplot(pressure, aes(x=temperature, y=pressure)) + geom_line()

ggplot(pressure, aes(x=temperature, y=pressure), ylim=400) + geom_line()

ggplot(pressure, aes(x=temperature, y=pressure)) + geom_line() + geom_point()
barplot(BOD$demand)
barplot(BOD$demand, main="graph of demand")
barplot(BOD$demand, main="graph of demand", names.arg = BOD$time)
cylcount <- table(mtcars$cyl)

barplot(cylcount)

qplot(mtcars$cyl, bins=30)

qplot(factor(mtcars$cyl))
ggplot(mtcars, aes(factor(cyl))) + geom_bar()
boxplot(mtcars$mpg)

# color
rainbow(5)
heat.colors(5)
terrain.colors(5)
topo.colors(5)
cm.colors(5)

mycolors <- rainbow(3)
mycolors2 <- heat.colors(3)
ggplot(mtcars, aes(x=factor(cyl))) + geom_bar(fill=mycolors)
ggplot(mtcars, aes(x=factor(cyl))) + geom_bar(fill=rainbow(3))
barplot(BOD$demand, col=rainbow(6))
barplot(BOD$demand, col="royalblue3")

testscores <- c(96, 71, 85, 92, 82, 78, 72, 81, 68, 61, 78, 86, 90)
barplot(testscores)
barplot(testscores, col="blue")

testcolors <- ifelse(testscores >= 80, "blue", "red")
barplot(testscores, col="blue")

barplot(testscores, col=testcolors, main = "Test scores")
barplot(testscores, col=testcolors, main = "Test scores", ylim=c(0,100))
barplot(testscores, col=testcolors, main = "Test scores", ylim=c(0,100), las=1)

testscores <- sort(c(96, 71, 85, 92, 82, 78, 72, 81, 68, 61, 78, 86, 90), decreasing = TRUE)
barplot(testscores, col=testcolors, main = "Test scores descending")

qplot(factor(cyl), data=mtcars, geom="bar", fill=factor(cyl))

x <- 11
print(x)

