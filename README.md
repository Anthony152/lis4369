> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible Enterprise Solutions

## Anthony Threlfall

### LIS4369 Requirements:

*Course Work Links:*

*Assignment:*

1. [A1 README.md](a1/README.md "My A1 README.md file")
    - Install Python
    - Install R
    - Install R Studio
    - Install Visual Studio Code
    - Create "a1_tip_calculator" application
    - Create "a1 tip calculator" Jupyter Notebook
    - Provide screenshots of installations
    - Create Bitbucket repo
    - Complete Bitbucket tutorial (bitbucketstationlocations)
    - Provide git command descriptions
2. [A2 README.md](a2/README.md "My A2 README.md file")
    - Create Payroll app using "Separation of Concerns" design principles
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skills set
3. [A3 README.md](a3/README.md "My A3 README.md file")
    - Create Painting Estimator using "Separation of Concerns" design principles
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skills set
        - Calorie Percentage
        - Python Selection Structures
        - Python Loops
4. [A4 README.md](a4/README.md "My A4 README.md file")
    - Create demo.py
    - Create Data Analysis 2
    - Provide screenshots of completed app
    - Provide screenshots of completed Python skills set
        - Using Dictionaries
        - Random Number Generator
        - Temperature Conversion Program
5. [A5 README.md](a5/README.md "My A5 README.md file")
    - Complete the tutorial: Introduction_to_R_Setup_and_Tutorial
        - Create and save: learn_to_use_r.R
    - Code and run lis4369_a5.R
        - include two plots
    - Provide screenshots of completed Python skills set
        - Sphere_Volume_Calculator
        - Calculator_with_Error_Handling
        - File_Write/Read

*Projects*

1. [Project 1](project1/README.md "My project 1 README.md file")
    - Create demo.py
    - Create Data Analysis 1	
    - Provide screenshots of completed app
    - provide screenshots of completed Python skills set
        - Using Lists
        - Using Tuples
        - Using Sets

2. [Project 2](project2/README.md "My project 2 README.md file")
    -  Code and run lis4369_p2_requirements.R
        - Provide two plots
    - Code and run lis4369_soads.R
        - Provide two plots
