import random

def get_requirements():

    print("Developer Anthony Threlfall")

    print("Pseudo-Random Number Generator")
    print("\nProgram Requirements:\n"
    + "1. Get user beginning and ending integer values, and store in two variables.\n"
    + "2. Display 10 pseudo-random numbers between, and including, above values.\n"
    + "3. Must use integer data types.\n"
    + "4. Example 1: Using range() and randint() functions.\n"
    + "5. Example 2: Using a list with range() and shuffle() functions.\n")

def random_numbers():
    # initialize variables
    start = 0
    end = 0
    
    # get user data
    print("Input:")
    start = int(input("enter beginning value: "))
    end = int(input("enter ending value: "))

    print("\nOutput:")
    print("Example1: Using range() and randint() functions:")
    for count in range(10):
        print(random.randint(start, end), sep=", ", end=" ")
    
    print()

    print("\nExample 2: Using a list, with range() and shuffle() functions")
    # shuffle() randomizes items of a list in place (must incled list)
    # range generates list of intergers from 1 but not including 11
    r = list(range(start, end + 1))
    random.shuffle(r)
    for i in r:
        print(i, sep= ", ", end= " ")
    
    print()