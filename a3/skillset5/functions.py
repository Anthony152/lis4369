def get_requirements():
    print("Developer: Anthony Threlfall")
    print("Python selection structures")
    print("\nProgram Requirements:\n"
        + "1. Use python selection\n"
        + "2. Prompt user for two numbers, and a suitable operator.\n"
        + "3. Test for correct numeric operator.\n")

def get_input():
    print("Python Calculator")    
    num1 = float(input("Enter num1: "))
    num2 = float(input("Enter num2: "))
    print("Suitable Operators: +, -, *, /, // (interger division), % (modulo operator), ** (power)")
    oper = str(input("Enter operator: "))
    return num1, num2, oper

def calculate(num1, num2, oper):
    x = 0
    if oper == "+":
        result = num1 + num2

    elif oper == "-":
        result = num1 - num2

    elif oper == "*":
        result = num1 * num2

    elif oper == "/":
        result = num1 / num2

    elif oper == "//":
        result = num1 // num2
    
    elif oper == "%":
        result = num1 % num2
    
    elif oper == "**":
        result = num1 ** num2

    else:
        print("Incorrect operator!")
        x=1
    
    if x != 1:
        print(result)
