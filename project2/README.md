> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Project 2 Requirements:

*Four Parts:*

1. Create and runlearn_to_use_r.R
2. Create and run lis4369_a5.R
3. R Language Questions
4. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of lis4369_p2.R and two graphs
* Screenshots of lis4369_soads.R and two graphs


#### Assignment Screenshot:

*lis4369_p2.R and two graphs*

| ![lis4369_p2.R](img/p2_1.png "lis4369_p2.R") | ![lis4369_p2.R](img/p2_2.png "lis4369_p2.R") |
| -------------------- | ------------------------------ | 
| ![lis4369.R graph](img/plot_disp_and_mpg_1.png "lis4369_p2.R graph") | ![lis4369_p2.R graph](img/plot_disp_and_mpg_1.png "lis4369_p2.R graph") |


*lis4369_soads.R:*
![lis4369_soads.R](img/soads.png "lis4369_soads.R")

| ![lis4369_soads.R graph](img/gs1.png "lis4369_soads.R graph") | ![lis4369_a5.R graph](img/gs2.png "lis4369_soads.R graph") |
| ------------ | ------------ |
|              |              |

