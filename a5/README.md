> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 5 Requirements:

*Four Parts:*

1. Create and runlearn_to_use_r.R
2. Create and run lis4369_a5.R
3. Chapter Questions (Chs 11 and 12)
4. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of learn_to_use_r.R and two graphs
* Screenshots of lis4369_a5.R and two graphs
* Screenshots of SkillSet (13, 14, 15)


#### Assignment Screenshot:

*learn_to_use_r.R and two graphs*
![learn_to_use_r.R](img/ltu.png "learn_to_use_r.R") 

| ![learn_to_use_r.R graph](img/ltu1.png "learn_to_use_r.R graph") | ![learn_to_use_r.R graph](img/ltu2.png "learn_to_use_r.R graph") |
| ---------- | ---------- | 
|            |            |

*lis4369_a5.R:*
![lis4369_a5.R](img/a5.png "lis4369_a5.R")

| ![lis4369_a5.R graph](img/a5_1.png "lis4369_a5.R graph") | ![lis4369_a5.R graph](img/a5_2.png "lis4369_a5.R graph") |
| ------------ | ------------ |
|              |              |

*Skreenshots of Python Skill Sets:*

| Skill Set 13: Sphere Volume Calculator | SkillSet 14: Calculator with Error Handling | Skill Set 15: File Write/Read |
| ----- | ----- | ----- |
| ![Skill Set 13](img/ss13.png "Sphere Volume Calculator") | ![Skill Set 14](img/ss14.png "Calculator with error handling") | ![Skill Set 15](img/ss15.png "File Write/Read") |

