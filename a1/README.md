> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 1 Requirements:

*Four Parts:*

1. Distributed Version Control with Git and Bitbucket
2. Development Installations
3. Questions
4. bitbucket repo links:

    a) this assignment and

    b) the completed tutorial (bitbucketstationlocation)

#### README.md file should include the following items:

* Screenshot of a1_tip_calculator application running
* Link to A1.jpynb file: [tip_calculator.jpynb](a1_tip_calculator/tip_calculator.ipynb "A1 Jupyter Notebook")
* Git commands w/short descriptions


> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>
> #### Git commands w/short descriptions:

1. git init - Create an empty Git repository or reinitialize an existing one.
2. git status - Show the working tree status.
3. git add - Add file contents to the index.
4. git commit - Record changes to the repository.
5. git push - Update remote refs along with associated objects.
6. git pull - Fetch from and integrate with another repository or a local branch.
7. git rm - Remove files from the working tree and from the index.
 

#### Assignment Screenshots:

#### Screenshot of a1_tip_calculator application running (IDLE):

![Python Installation Screenshot IDLE](img/a1_tip_calculator_idle.png "A1 IDLE Screenshot")

#### Screenshot of a1_tip_calculator application running (Visual Studio Code):
![Python Insallation Screenshot VS Code](img/a1_tip_calculator_vs_code.png "A1 VS Code Screenshot")

#### A1 Jupyter Notebook:
![tip_calculator.ipynb](img/a1_jupyter_notebook.png "A1 Jupyter Notebook")

#### Tutorial Links:

*Bitbucket Tutorial - Station Locations:*
[A1 bitbucket station Locations Tutorial Link](https://bitbucket.org/Anthony152/bitbucketstationlocations/ "Bitbucket Station Locations")
