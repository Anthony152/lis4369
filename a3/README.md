> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 3 Requirements:

*Three Parts:*

1. Painting Estimator App
2. Chapter Questions (Chs 6)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running Painting Estimator app
* Screenshots of Python skill sets
* Link to A3.jpynb file: [painting_estimator.jpynb](painting_estimator/painting_estimator.ipynb "A3 Jupyter Notebook") 


#### Assignment Screenshot:

#### Painting Estimator App
![Painting Estimator App](img/painting_estimator.png "Painting Estimator") 

*Skreenshots of Python Skill Sets:*

| Skill Set 4: Calorie Percentage | SkillSet 5: Python Selection Structures | Skill Set 6: Python Loops |
| ----- | ----- | ----- |
| ![Skill Set 4](img/ss4.png "Calorie Percentage") | ![Skill Set 5](img/ss5.png "Python Selection Structures") | ![Skill Set 6](img/ss6.png "Python Loops") |

#### A3 Jupyter Notebook:
| ![painting_estimator.ipynb](img/a3_jupyter_notebook1.png "A3 Jupyter Notebook") | ![painting_estimator.ipynb](img/a3_jupyter_notebook2.png "A3 Jupyter Notebook") |
| ----- | ----- |
|  |  |
