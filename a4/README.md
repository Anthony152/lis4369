> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 4 Requirements:

*Five Parts:*

1. Create Demo.py
2. Data Analysis 2 App
3. Complet Python skills set
4. Chapter Questions (Chs 9 and `10)
5. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running demo.py and code.
* Screenshot of running Data Analysis 2 app
* Screenshots of Python skill sets
* Link to project1.jpynb file: [data_analysis2.jpynb](data_analysis2/data_analysis_2.ipynb "Assignment 4 Jupyter Notebook") 


#### Project Screenshot:

#### Demo.py code

| ![Demo.py code](img/demo1.png "Demo.py code") | ![Demo.py code](img/demo2.png "Demo.py code")  | ![Demo.py code](img/demo3.png "Demo.py code")  | ![Demo.py code](img/demo4.png "Demo.py code") |
| -------------------- | --------------------- | ---------------------- | -------------- |
|                      |                       |                        |                |

#### Running Demo.py

| ![Running demo.py](img/rdemo1.png "Running Demo.py") | ![Runnning Demo.py](img/rdemo2.png "Running Demo.py") |
| -------------------- | --------------------- |
|                      |                       |

#### Data Analysis 2 App
|               |              |                |
| ------------- | ------------ | -------------- |
| ![data analysis 2 App](img/data2.1.png "Data Analysis 2") | ![data analysis 2 app](img/data2.2.png "Data Analysis 2") | ![data analysis 2 graph](img/graph.png "Data Analysis 2 graph") |

*Screenshots of Python Skill Sets:*

| Skill Set 10: Using Dictionaries | SkillSet 11: Random Number Generator | Skill Set 12: Temperature Conversion Program |
| ----- | ----- | ----- |
| ![Skill Set 10](img/ss10.png "Using Dictionaries") | ![Skill Set 11](img/ss11.png "Random Number Generator") | ![Skill Set 12](img/ss12.png "Temperature Conversion Program") |

#### Assignment 4 Jupyter Notebook:
| ![data_analysis.ipynb](img/jupyter1.png "Assignment 4 Jupyter Notebook") | ![data_analysis.ipynb](img/jupyter2.png "Assignment 4 Jupyter Notebook") | ![data_analysis.ipynb](img/jupyter3.png "Assignment 4 Jupyter Notebook") |
| ----- | ----- | ---- |
| ![data_analysis.ipynb](img/jupyter4.png "Assignment 4 Jupyter Notebook") | ![data_analysis.ipynb](img/jupyter5.png "Assignment 4 Jupyter Notebook") |   |
