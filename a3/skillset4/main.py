import functions as f

def main():
    f.get_requirements()
    f.calorie_percentage()

if __name__ == "__main__":
    main()