> **NOTE:** This README.md file should be placed at the **root of each of your repos directories.**
>
>Also, this file **must** use Markdown syntax, and provide project documentation as per below--otherwise, points **will** be deducted.
>

# LIS4369 - Extensible enterprise solutions

## Anthony Threlfall

### Assignment 2 Requirements:

*Three Parts:*

1. Payroll App
2. Chapter Questions (Chs 3,4)
3. Bitbucket repo link

#### README.md file should include the following items:

* Screenshot of running payroll app
* Screenshots of Python skill sets
* Link to A2.jpynb file: [payroll_calculator.jpynb](payroll_calculator/PayrollCalculator.ipynb "A2 Jupyter Notebook") 

> This is a blockquote.
> 
> This is the second paragraph in the blockquote.
>

#### Assignment Screenshots:

*Screenshots of Payroll App:*

| Payroll No Overtime | Payroll with Overtime |
| ----- | -------- |
| ![Payroll App No Overtime](img/payroll_no_overtime.png "Payroll No Overtime") | ![Payroll App With Overtime](img/payroll_with_overtime.png "Payroll with Overtime") |

*Skreenshots of Python Skill Sets:*

| Skill Set 1: Square Feet to Acres | SkillSet 2: Miles Per Gallon | Skill Set 3: IT/ICT student Percentage |
| ----- | ----- | ----- |
| ![Skill Set 1](img/skillset1.png "Square Feet to Acres") | ![Skill Set 2](img/skillset2.png "Miles Per Gallon") | ![Skill Set 3](img/skillset3.png "IT/ICT Student Percentage") |

#### A2 Jupyter Notebook:
![PayrollCalculator.ipynb](img/a2_jupyter_notebook1.png "A2 Jupyter Notebook")


![PayrollCalculator.ipynb](img/a2_jupyter_notebook2.png "A2 Jupyter Notebook")
